
$(document).ready(function () {
    deliveryApp.barStyleClick();
    deliveryApp.fullCalendar(function (startDay, endDay) {
        console.log('OK',startDay, endDay);
    });
});


var deliveryApp = (function ($) {
    var barStyleClick = function (text) {
        $('.bar-option--icon').on('click', function(e) {
            e.preventDefault();
            if($(this).hasClass('active')) return false;
            $(this).addClass('active');
            $(this).siblings().removeClass('active');
            if($(this).hasClass('_week') || $(this).hasClass('_month')) {
                $('.control-week, .container_row').addClass('hasWeek');
            }else {
                $('.control-week, .container_row').removeClass('hasWeek');
            }

            e.stopPropagation();
        })

    };
    var onChangeDate = function() {};
    var fullCalendar = function(callback) {
        var intervalStart, intervalEnd, _type = '';
        $('#calendar').fullCalendar({
            defaultDate: moment().format('YYYY-MM-DD'),
            header: {
                left: 'prevYear,prev,next,nextYear',
                center: 'title',
                right: 'month,agendaWeek,listWeek'
            },
            viewRender: function (view, element)
            {
                intervalStart = view.intervalStart.format('YYYY-MM-DD');
                intervalEnd = view.intervalEnd.format('YYYY-MM-DD');
                console.log('week_start',view.type, intervalStart, intervalEnd);
                switch (view.type) {
                    case 'listWeek':
                        $('.control-week, .container_row').removeClass('hasWeek');
                        $('#calendar').addClass('hid');
                        break;
                    default:
                        $('.control-week, .container_row').addClass('hasWeek');
                        $('#calendar').removeClass('hid');
                        break;
                }
                try {
                    callback(intervalStart, intervalEnd)
                }catch (e) {
                    console.log(e.message)
                }
            },
            events: function(start, end, timezone, callback) {
                // console.log('OOP',{_type, start, end, callback})
            }
        });
    }
    window.deliveryApp = deliveryApp;
    return {
        barStyleClick: barStyleClick,
        onChangeDate: onChangeDate,
        fullCalendar: fullCalendar
    };
})($);


var isMobile = {
    Android: function () {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        return navigator.userAgent.match(/IEMobile/i) || navigator.userAgent.match(/WPDesktop/i);
    },
    any: function () {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

